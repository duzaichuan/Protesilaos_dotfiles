#!/bin/sh

# remove "unread" from "replied"
notmuch tag -unread -new -- tag:replied

# post-new --- Notmuch rules that run after notmuch new
#
# Copyright (c) 2021  Protesilaos Stavrou <info@protesilaos.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
## Commentary:
#
# The order of those commands matters.  Maybe I will write something
# more sophisticated in the future.
#
# Part of my dotfiles: <https://gitlab.com/protesilaos/dotfiles>.


# tag all "new" messages "inbox" and "unread"
notmuch tag +inbox +unread -new -- tag:new

# tag my replies as "sent"
notmuch tag -new -unread +inbox +sent -- 'from:"/(Protesilaos Stavrou)?*@protesilaos.com*/"'

# mailing lists
notmuch tag -inbox +list +emacs -- from:emacs-devel@gnu.org or to:emacs-devel@gnu.org or subject:[emacs-devel]
notmuch tag -inbox +list +emacs -- from:emacs-orgmode@gnu.org or to:emacs-orgmode@gnu.org or subject:[emacs-orgmode]
notmuch tag -inbox +list +emacs -- 'to:"/*@debbugs.gnu.org*/"'
notmuch tag -inbox +list +emacs -- from:emacs-humanities@gnu.org or to:emacs-humanities@gnu.org or subject:[emacs-humanities]
